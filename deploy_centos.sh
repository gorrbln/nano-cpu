RNAME=`hostname`
sed -i -e "s/FinG/${RNAME}/g" 1.1.1/config.ini

cp 1.1.1/nanocpu /usr/sbin/
cp 1.1.1/nanolog /usr/sbin/
cp 1.1.1/nanocpu.service /etc/systemd/system/
cp 1.1.1/config.ini /usr/sbin/

yum install -y ccze tzdata

systemctl enable nanocpu
systemctl start nanocpu