#!/usr/bin/env bash

VERSION=`sed -n 's/Version: \(.*\)/\1/ip;T;q' last_version `
BUILD_PATH=/tmp/nano-cpu-pasc
PATH_BIN=/tmp/nano-cpu-pasc/usr/local/bin/miners/nano-cpu-pasc_$VERSION
PATH_DEBIAN=/tmp/nano-cpu-pasc/DEBIAN
PATH_SYSTEMD=/tmp/nano-cpu-pasc/lib/systemd/system


mkdir -p $PATH_BIN && mkdir -p $PATH_DEBIAN && mkdir -p $PATH_SYSTEMD

cp $VERSION/* $PATH_BIN
cp $VERSION/nanocpu /usr/local/bin

cp $VERSION/control $PATH_DEBIAN
cp $VERSION/dirs $PATH_DEBIAN
cp $VERSION/nanocpu.service $PATH_SYSTEMD

cd $BUILD_PATH

dpkg-deb -b ./ ./
